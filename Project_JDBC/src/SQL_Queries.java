import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import DBConnection.*;


public class SQL_Queries {
	
	public static void main(String args[])
	{
		Run_queries obj = new Run_queries();
		obj.getResult();		
	}	
}

class Run_queries
{
	Connection con = null;
	Statement  stmt = null;
	
	public void getResult()
	{
		Scanner c = new Scanner(System.in);
		con = SQL_Connection.getDBconnection();
		
		try {
			//select records from authors table with ascending order of first name and last name
			System.out.println("Data in the Authors table");
			System.out.println("");
			String query = "SELECT * FROM AUTHORS ORDER BY LASTNAME ASC, FIRSTNAME ASC";
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			System.out.println("The data of AUTHORS table");
			System.out.println(" ");
			System.out.println("Authors ID"+"  "+"First Name"+"      "+"Last Name");
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"           "+rs.getString(2)+" "+rs.getString(3));
			}
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//select records from publishers table 
			System.out.println("Data in the Publishers table");
			System.out.println("");
			String query2 = "SELECT * FROM PUBLISHERS";
			ResultSet rs2 = stmt.executeQuery(query2);
			
			System.out.println("The data of PUBLISHERS Table");
			System.out.println("");
			System.out.println("Publisher ID"+" "+"Publisher Name");
			while(rs2.next())
			{
				System.out.println(rs2.getString(1)+"           "+rs2.getString(2));
			}
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//select records for specific publisher
			System.out.println("Record for specific publisher");
			System.out.println(" ");
			System.out.println("Enter the publisher name for which record needs to be selected");
			String publisherName2 = c.nextLine();
			
			String query3 = "SELECT PUBLISHERS.PUBLISHERNAME,TITLES.TITLE,TITLES.COPYRIGHT,TITLES.ISBN FROM TITLES INNER JOIN PUBLISHERS ON TITLES.PUBLISHERID = PUBLISHERS.PUBLISHERID WHERE PUBLISHERNAME = '"+publisherName2+"' ORDER BY TITLE" ;
			ResultSet rs3 = stmt.executeQuery(query3);
			System.out.println("Publisher Name"+"  "+"Title"+"         "+"Copyright"+"   "+"ISBN");
			while(rs3.next())
			{
				System.out.println(rs3.getString(1)+"      "+rs3.getString(2)+"       "+rs3.getString(3)+"        "+rs3.getString(4));
			}
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//insert into authors table
			System.out.println("Insert data into AUTHORS table");
			System.out.println("");
			System.out.println("Enter Authors first name");
			String firstName = c.nextLine();
			System.out.println("Enter Authors last name");
			String lastName = c.nextLine();
			
			String query4 = "INSERT INTO AUTHORS (AUTHORID, FIRSTNAME, LASTNAME) VALUES(SEQ_AUTHORID.NEXTVAL,'"+firstName+"','"+lastName+"')";
			int updatedRows=stmt.executeUpdate(query4);
			System.out.println(updatedRows +" row created in AUTHORS table");
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//update authors information
			System.out.println("Update information of Authors table");
			System.out.println("");
			System.out.println("Enter the Aurthor ID for which update needs to be done");
			Integer authorID = Integer.parseInt(c.nextLine());
			System.out.println("Enter Authors first name which needs to be updated");
			String firstName1 = c.nextLine();
			
			String query5 = "UPDATE AUTHORS SET FIRSTNAME = '"+firstName1+"' WHERE AUTHORID = "+authorID+"";
			int updatedRows2 = stmt.executeUpdate(query5);
			System.out.println(updatedRows2 +" row updated in AUTHORS table");
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//Add new title for an author
			System.out.println("Add new title for author");
			System.out.println("");
			System.out.println("Enter the Author ID for which title needs to be added");
			int authorID2 = Integer.parseInt(c.nextLine());
			System.out.println("Enter the new Title");
			String title = c.nextLine();
			System.out.println("Enter the new Title book number (ISBN)");
			String ISBN = c.nextLine();
			
			String query6 = "INSERT INTO AUTHORISBN (AUTHORID,ISBN) VALUES("+authorID2+",'"+ISBN+"')";
			int updatedRows3 = stmt.executeUpdate(query6);
			System.out.println(updatedRows3 +" row added in AuthorISBN table");
			
			String query7 = "INSERT INTO TITLES (ISBN,TITLE,EDITIONNUMBER,COPYRIGHT,PUBLISHERID,PRICE) VALUES('"+ISBN+"','"+title+"',3,'2014',4,142.56)";
			int updatedRows4 = stmt.executeUpdate(query7);
			System.out.println(updatedRows4 +" row added in TITLES table");
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			
			//Add new publisher
			System.out.println("Add new Publisher");
			System.out.println("");
			System.out.println("Enter new publisher name");
			String publisherName = c.nextLine();
			
			String query8 = "INSERT INTO PUBLISHERS (PUBLISHERID,PUBLISHERNAME) VALUES(SEQ_PUBLISHERID.NEXTVAL,'"+publisherName+"')";
			int updatedRows5=stmt.executeUpdate(query8);
			System.out.println(updatedRows5 +" row created in PUBLISHERS table");
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
			//Edit/Update the existing information about a publisher
			System.out.println("Update information of PUBLISHERS table");
			System.out.println("");
			System.out.println("Enter the Publisher ID");
			Integer publisherID = Integer.parseInt(c.nextLine());
			System.out.println("Enter the new Publisher Name");
			String publisherName1 = c.nextLine();
			
			String query9 = "UPDATE PUBLISHERS SET PUBLISHERNAME = '"+publisherName1+"' WHERE PUBLISHERID = "+publisherID+"";
			int updatedRows6 = stmt.executeUpdate(query9);
			System.out.println(updatedRows6 +" row updated in PUBLISHERS table");
			System.out.println(" ");
			System.out.println("-------------------------------------------------------------------------------------");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}



